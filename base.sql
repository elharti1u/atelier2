# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Hôte: 127.0.0.1 (MySQL 5.6.22)
# Base de données: locatphoto
# Temps de génération: 2015-02-13 00:07:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table city
# ------------------------------------------------------------

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;

INSERT INTO `city` (`id`, `name`, `lat`, `lng`)
VALUES
	(1,'Nancy',48.6921,6.18442),
	(2,'Paris',48.8566,2.35222),
	(3,'Nantes',47.2184,-1.55362),
	(4,'Fes',34.0333,-5);

/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part`;

CREATE TABLE `part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo_user` varchar(254) COLLATE utf8_unicode_ci DEFAULT '',
  `score` int(11) DEFAULT NULL,
  `state` varchar(254) COLLATE utf8_unicode_ci DEFAULT '',
  `idCity` int(11) DEFAULT NULL,
  `dateC` date DEFAULT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `niveau` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCity` (`idCity`),
  CONSTRAINT `part_ibfk_1` FOREIGN KEY (`idCity`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `part` WRITE;
/*!40000 ALTER TABLE `part` DISABLE KEYS */;

INSERT INTO `part` (`id`, `pseudo_user`, `score`, `state`, `idCity`, `dateC`, `token`, `niveau`)
VALUES
	(33,'ayoubss',NULL,'',1,NULL,NULL,NULL),
	(105,'ayoub',NULL,'',1,NULL,'$1$PziG.Ar5$mwJmbYEb0M.9U8hH8NjVB1',NULL),
	(106,'ayoub',828,'termine',1,NULL,'$1$sZcal4al$sf3BUdBnKFV1.gfB4n9U91',NULL),
	(107,'ayoub',2212,'termine',1,NULL,'$1$ldwAebv7$ScjvMWBcUZQxnaBwzBDUl.',NULL),
	(108,'ayoub',NULL,'',1,NULL,'$1$N6Ji/pXx$jFyJTkZ/zL5/3Xe1VEivE.',NULL),
	(109,'ayoub',132,'termine',2,NULL,'$1$a0mYh8fF$t3tNK16q0U9pY88Q4ZYNt.',NULL),
	(110,'',NULL,'',1,NULL,'$1$obC91zcU$Tbbks9RpJCmm6pJ65pPj50',NULL),
	(111,'ayoub',140,'termine',1,NULL,'$1$Szn0P80g$elVWDXHcq7TJ2ylNG6f99.',NULL),
	(112,'dddsas',14,'termine',1,NULL,'$1$/T2mvuvU$mrY4CGa7xIR0npyk4lZmd1',NULL),
	(113,'ayoub',20,'termine',1,NULL,'$1$lSuS6hcj$HJddX.v1xeJ5p1/lkgJ910',NULL),
	(114,'ayoub',NULL,'',1,NULL,'$1$nteIM85Q$A5CVyFLN6v3QPDarcZaJ6.',2),
	(115,'ayoub',14,'termine',1,NULL,'$1$lFccV9EZ$jyn/7REuQ48.HyYTQFTUK/',1),
	(116,'ayoub',0,'termine',1,NULL,'$1$KP7VrJn2$hI2x69HVenfxfZek4Hkun/',2),
	(117,'ayoub',NULL,'',1,NULL,'$1$T8UFqCik$Pvw/5wovekkw6YNn.Dyes1',1),
	(118,'ayoub',NULL,'',1,NULL,'$1$3w3A599N$J2vCSzGzyaI8ScRnRPHV51',1),
	(119,'ayoub',NULL,'',1,NULL,'$1$UrC0VxfO$ks3EKu5HRGARv.SeDlurD0',1),
	(120,'ayoub',NULL,'',1,NULL,'$1$VkVg/hkv$ZOnKoj/33o3LzeQYuAeG9/',1),
	(121,'ayoub',0,'termine',1,NULL,'$1$BrvxrOH6$0vltWkhXJ4Hhbt64pIwrk/',1),
	(122,'ayoub',NULL,'',1,NULL,'$1$3.tnY9Vn$.8kjQmo1333ri6yS/HR2l1',1),
	(123,'ayoub',NULL,'',2,NULL,'$1$JOiZBa18$.ihm/Vr67c2.W7cxKTR1j1',1),
	(124,'ayoub',6,'termine',1,NULL,'$1$1hx6PFUZ$4E0MZnxGLkiMK43G/OooF/',1),
	(125,'ayoub',NULL,'',1,NULL,'$1$40/K/bjz$7orQt8gAlqgvwDfQ9d34p.',1),
	(126,'ayoub',NULL,'',1,NULL,'$1$amujN3LO$FBbO/iqlwYSYW7OUaBVS91',1),
	(127,'ayoub',NULL,'',1,NULL,'$1$8hohz5Bc$.ZkHklcwvvBI6EGpLp5t31',1),
	(128,'ayoub',20,'termine',1,NULL,'$1$gXfDGiXn$3TVPnRUuvBNXPsLmPJafm1',1),
	(129,'ayoub',0,'termine',1,NULL,'$1$qQmKJmE5$mTv6Aa1YmDQAT4HqOetWe1',1),
	(130,'ayoub',0,'termine',1,NULL,'$1$gQzsV0xi$X656uQB/FxeZep1JPPnzM.',1),
	(131,'ayoub',NULL,'',1,NULL,'$1$BOTiZZbY$itURQyjMPkdwT3091oPzC.',1),
	(132,'ayoub',0,'termine',1,NULL,'$1$sqIMgh2M$vGJ3gW8Cp8RLUDRpRzbOG0',1),
	(133,'ayoub',NULL,'',1,NULL,'$1$az2bGcvo$obdEyrph1LjhdcPEJ1pUn.',1),
	(134,'ayoub',0,'termine',1,NULL,'$1$IHI8tf1Y$GhkS8mki0Ccw8BcDasI9C/',1),
	(135,'ayoub',NULL,'',1,NULL,'$1$zk10gLbu$iJLXXg1qrMtsy5sAvHHch.',1),
	(136,'ayoub',NULL,'',1,NULL,'$1$ep4P1GIx$NwfgOLLzqVMPq5L.0V2wO0',1),
	(137,'ayoub',0,'termine',1,NULL,'$1$STaLHcMK$w.PtXCQb73CmdkcufCfqR0',1),
	(138,'ayoub',0,'termine',1,NULL,'$1$IApnCYQD$xSkjp1Ulht.uYa2MRr7JE1',1),
	(139,'ayoub',0,'termine',1,NULL,'$1$iKws3M/1$VzaLFdtgf69mfZ6SndcQo0',1),
	(140,'ayoub',0,'termine',1,NULL,'$1$6SGLwcbf$11fAFA.Rm2XR67YRLFwFC1',1),
	(141,'ayoub',NULL,'',1,'2015-02-12','$1$kz61az7n$P3A0AJLT3FyqU4LApc5bM1',1),
	(142,'ayoub',NULL,'',1,'2015-02-12','$1$FdizBArX$s6q1mAgbJIObwEcbrEE6A.',1),
	(143,'ayoub',0,'termine',1,'2015-02-12','$1$CFmthegB$hDMuzfoDzLqNjt5u8VxEP/',1),
	(144,'dddsas',NULL,'',1,'2015-02-12','$1$Md56v5o9$XUkL8sr35aRybraQPghTk.',1),
	(145,'dddsas',0,'termine',1,'2015-02-12','$1$o/23qofT$0Wy1pz/Msbr5Q1hgjghsx0',1),
	(146,'dddsas',NULL,'',1,'2015-02-12','$1$KmbJLwRP$OjBiskNqf8PnihWh9VBSh0',1),
	(147,'ayoub',0,'termine',1,'2015-02-12','$1$iGNtKzv.$YxKshaC.LAB.bJMIbi9OV/',1),
	(148,'dddsas',NULL,'',1,'2015-02-12','$1$jzXZdGrW$sRrvm376BMtm9ZbywrEqq.',1),
	(149,'ayoub',NULL,'',1,'2015-02-12','$1$AV6EpFcj$K7AXi/nDY0Q19XmTvSTEW/',1),
	(150,'ayoub',0,'termine',1,'2015-02-12','$1$26EvgP4Z$wu1kKkJV1kOaVefh5GUWp0',1),
	(151,'ayoub',NULL,'',1,'2015-02-12','$1$dLsyQJKl$YbP0mYOjPrrmvL5LCI4Pq/',1),
	(152,'ayoub',NULL,'',1,'2015-02-12','$1$gBAwVGAY$OfaTmB1HLSixR3PnI9W0A/',1),
	(153,'ayoub',0,'termine',1,'2015-02-12','$1$Mp633qXn$mAHdSVYmPDsYcVF6xGjWe/',1),
	(154,'ayoub',0,'termine',2,'2015-02-12','$1$N.DuzRgt$AbmB4kCQ/Wp1LveURCR701',1),
	(155,'ayoub',0,'termine',1,'2015-02-12','$1$8FjmzR2.$YSSKlS6EzvoGCueNKOmn51',1),
	(156,'ayoub',0,'termine',1,'2015-02-12','$1$R7af9GZy$4Pd.qFAxHWatAJoZp49TH.',1),
	(157,'ayoub',NULL,'',1,'2015-02-12','$1$.PawGVVk$YEiriGUhaO9TAlHxaA4kQ/',1),
	(158,'ayoub',NULL,'',1,'2015-02-12','$1$HpnZwd5y$rBqoHXPq0B0TSOTAWN1D21',1),
	(159,'ayoub',0,'termine',1,'2015-02-12','$1$hMpi5GQ5$ik8C62MSSvAPcbiJc4hXB.',1),
	(160,'ayoub',0,'termine',1,'2015-02-12','$1$MP2HwiXU$K9HRyRrOGU3z8TQe3UXK2.',1),
	(161,'ayoub',0,'termine',2,'2015-02-12','$1$MsY.1iZC$UE1MkGCPWuoydRNkfTyIY1',1),
	(162,'ayoub',NULL,'',1,'2015-02-12','$1$lK1ylIkz$pVnGEfWqUxqB4nWCn3eTH/',1),
	(163,'ayoub',NULL,'',1,'2015-02-12','$1$znHFkkOu$xitMBwt4t1tTvFBcI.UCA.',1),
	(164,'ayoub',378,'termine',1,'2015-02-12','$1$Nu3XNcRp$liURG/a1oK4UTRk8BaunM.',1),
	(165,'ayoub',6812,'termine',1,'2015-02-12','$1$QUMoUsRl$IWvIi3R6myGHaT4KEsWZt1',1),
	(166,'ayoub',548,'termine',1,'2015-02-12','$1$njXI/NLd$VcjNtjYs7Q9aU5uSugHCc0',1),
	(167,'ayoub',0,'termine',2,'2015-02-12','$1$P.a8dm4w$VzIk8qOhjuCKFMIgsd3jp.',1),
	(168,'ayoub',NULL,'',1,'2015-02-12','$1$KF9/9b0k$fV5OgvmHfCBdlrg8N.RG01',1),
	(169,'ayoub',NULL,'',1,'2015-02-12','$1$/42C4OL1$l3KPeORI43idPeRZ37TnD/',3),
	(170,'ayoub',0,'termine',4,'2015-02-12','$1$IckXYVzt$zDSD39AyjdHiFCvxrGesN/',1),
	(171,'ayoub',0,'termine',1,'2015-02-12','$1$FMR9uIyq$y4AN3qtBQxSLb3/Zdeizh0',1),
	(172,'ayoub',0,'termine',1,'2015-02-13','$1$gRwa6xaB$II1pVjsGRFkO23TXPhRbq0',1),
	(173,'ayoub',0,'termine',1,'2015-02-13','$1$mDmaGRUS$8eFKz10EZkAA6CDRzlDvr/',1);

/*!40000 ALTER TABLE `part` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `idCity` int(11) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  `dateA` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCity` (`idCity`),
  CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`idCity`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;

INSERT INTO `photo` (`id`, `name`, `url`, `lat`, `lng`, `idCity`, `idUser`, `dateA`)
VALUES
	(3,'Nancy place stan','App/template/images/4058049747.jpeg',48.6921,6.18442,1,4,NULL),
	(4,'Nancy','App/template/images/6777916935.jpeg',48.6921,6.18442,1,4,NULL),
	(5,'Nancy','App/template/images/9405293917.jpeg',48.6849,6.18746,1,4,NULL),
	(6,'Nancy','App/template/images/2069122296.jpeg',48.6899,6.17459,1,4,NULL),
	(7,'Nancy','App/template/images/8340394400.jpeg',48.6934,6.18202,1,4,NULL),
	(8,'Paris','App/template/images/4047371919.jpeg',48.8584,2.29448,2,4,NULL),
	(9,'Paris','App/template/images/1033141680.jpeg',48.8738,2.29503,2,4,NULL),
	(10,'Paris','App/template/images/5562373874.jpeg',48.8658,2.30731,2,4,'2015-02-12 00:00:00'),
	(11,'Nancy','App/template/images/8081283576.jpeg',48.6921,6.18442,1,4,'2015-02-12 00:00:00'),
	(12,'Fes','App/template/images/585769927.jpeg',34.0618,-4.98438,4,4,'2015-02-12 00:00:00');

/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `pseudo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `password`, `salt`, `pseudo`)
VALUES
	(4,'n36@hotmail.fr','73yJY7qCHFXAs','7301964','ayoub'),
	(8,'roi_2020@hotmail.fr','78TVFQk26OexE','7860966','ayoub'),
	(9,'ay.elharti@gmail.com','69pNK92Vh60Yc','6964929','ayoub'),
	(10,'ayoub.elharti@gmail.com','45./cb1p96CKI','4507151','ayoub'),
	(11,'n364@hotmail.fr','7508Wp7PmNxko','7534781','ayoubss');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
