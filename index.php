<?php

require_once 'vendor/autoload.php';
$app = new Slim\Slim();
session_start();


$app->get('/', function() {
    $c = new IndexC();
    $c->index();
})->name("home");

$app->group('/play', function() use($app) {

    $app->post("/", function() use ($app) {
        $c = new IndexC();
        $c->newPart($app);
    })->name("newPart");

    $app->get("/rest/games/:id", function($id) use ($app) {
        $c = new GameC();
        $c->getPartInfo($app, $id);
    })->name("getPartInfo");

    $app->put("/rest/games/:id", function($id) use ($app) {
        $c = new GameC();
        $c->updatePartInfo($app, $id);
    })->name("updatePartInfo");
    $app->get("/rest/games/:id/photos/", function($id) use ($app) {
        $c = new GameC();
        $c->getPartphotos($app, $id);
    })->name("getPartphotos");

    $app->get("/", function() {
        $c = new GameC();
        $c->index();
    });
});

$app->group("/admin/",function() use ($app){
    
 $app->get("login",  function ()use($app){
     $c=new AdminC();
     $c->index($app);
 })->name("pageLoginForm");  
 
 $app->post("login/",  function() use($app){
     $c=new AdminC();
     $c->login($app);
 })->name("ConnexionUrlForm");
 
  $app->get("inscription",  function() use($app){
     $c=new AdminC();
     $c->inscriptionView($app);
 })->name("inscriptionView");
 
   $app->post("inscription/",  function() use($app){
     $c=new AdminC();
     $c->inscription($app);
 })->name("inscriptionUrlForm");
 
$app->get('logout/',function() use ($app){
    $c=new AdminC();
    $c->logout($app);
})->name('logoutUrl');
 
 $app->get('addphoto/',function(){
     $c=new AddPhotoC();
     $c->index();

 })->name("addPhotos");
 
 $app->post('addphoto',function()use($app){
     $c=new AddPhotoC();
     $c->addPhoto($app);
 })->name("UrlAddPhoto");
 
 $app->get("topscore",  function (){
     $c=new ScoreC();
     $c->index();
 })->name("UrlScore");
});

$app->run();
