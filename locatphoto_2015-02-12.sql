# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Hôte: 127.0.0.1 (MySQL 5.6.22)
# Base de données: locatphoto
# Temps de génération: 2015-02-12 01:14:12 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table city
# ------------------------------------------------------------

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;

INSERT INTO `city` (`id`, `name`, `lat`, `lng`)
VALUES
	(1,'Nancy',48.6921,6.18442);

/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part`;

CREATE TABLE `part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo_user` varchar(254) COLLATE utf8_unicode_ci DEFAULT '',
  `score` int(11) DEFAULT NULL,
  `state` varchar(254) COLLATE utf8_unicode_ci DEFAULT '',
  `idCity` int(11) DEFAULT NULL,
  `dateC` date DEFAULT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCity` (`idCity`),
  CONSTRAINT `part_ibfk_1` FOREIGN KEY (`idCity`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `part` WRITE;
/*!40000 ALTER TABLE `part` DISABLE KEYS */;

INSERT INTO `part` (`id`, `pseudo_user`, `score`, `state`, `idCity`, `dateC`, `token`)
VALUES
	(33,'ayoubss',NULL,'',1,NULL,NULL),
	(34,'ayoubss',NULL,'',1,NULL,NULL),
	(65,'dddsas',NULL,'',1,NULL,'hTIB4/v7QXZD5FctMPtw'),
	(66,'dsd',NULL,'',1,NULL,'7ep72txAHjhPCBruD332'),
	(67,'dada',NULL,'',1,NULL,'/sHvD408EUJTgRcp2h8g'),
	(68,'ay',NULL,'',1,NULL,'iGVf8i36NRCrEIgMNCXk'),
	(69,'dddsas',NULL,'',1,NULL,'rnJoxFT2UZk4DJNOgFpQ'),
	(70,'dwdw',NULL,'',1,NULL,'7EVsot4Q6B5TESzLZchA'),
	(71,'sss',100,'1',1,NULL,'jRTYVSdEPCOaAhnO.Olb'),
	(91,'ayoub',NULL,'',1,NULL,'$1$JfroC13W$qjJBNcEEruJISkivlCqx71'),
	(92,'ayoub',NULL,'',1,NULL,'$1$qYcXy6UB$9BQTit/Lm7N.L2.xLItq..'),
	(93,'ayoub',NULL,'',1,NULL,'$1$E2mBVtqn$Ch5EQMjCvS8cALAgvGqbq.'),
	(94,'',NULL,'',1,NULL,'$1$8Ki6TiT2$ok4sSdw9db3C9ar/01.Dh0');

/*!40000 ALTER TABLE `part` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `idCity` int(11) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCity` (`idCity`),
  CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`idCity`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;

INSERT INTO `photo` (`id`, `name`, `url`, `lat`, `lng`, `idCity`, `idUser`)
VALUES
	(1,'photo nancy','www/url/wwwww/',1,3,1,NULL),
	(3,'Nancy place stan','App/template/images/4058049747.jpeg',48.6921,6.18442,1,4),
	(4,'Nancy','App/template/images/6777916935.jpeg',48.6921,6.18442,1,4),
	(5,'Nancy','App/template/images/9405293917.jpeg',48.6849,6.18746,1,4),
	(6,'Nancy','App/template/images/2069122296.jpeg',48.6899,6.17459,1,4);

/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `pseudo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `password`, `salt`, `pseudo`)
VALUES
	(4,'n36@hotmail.fr','73yJY7qCHFXAs','7301964','ayoub'),
	(8,'roi_2020@hotmail.fr','78TVFQk26OexE','7860966','ayoub'),
	(9,'ay.elharti@gmail.com','69pNK92Vh60Yc','6964929','ayoub'),
	(10,'ayoub.elharti@gmail.com','45./cb1p96CKI','4507151','ayoub'),
	(11,'n364@hotmail.fr','7508Wp7PmNxko','7534781','ayoubss');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
