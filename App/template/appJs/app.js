// Route    

var app = angular.module('MonApp', ['ngRoute']);
var score = 0 ;
var i = 0;
var secs;
var currentSeconds = 0;
var link;
var count;
var urlPut;
var niveau;
app.config(function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: dirname(location.pathname)+'/App/template/twig/game.html',
            controller: 'mapCtrl'
        })

});

app.config(function($interpolateProvider) {

    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});


//modifie table part 
function put(url){
    $.ajax({
            url: url+"&score="+score+"&state=termine",
            type: 'PUT',
            dataType: 'json',
            timeout: 1000,
            data:'',
            error: function(){console.log('error serveur'); },
            success: function(data) {             },
          });
}

//TImer
function timerStart() {
    timer = setTimeout('Decrement()', 1000);
}
function timerStop() {

    clearTimeout(timer);
}
function Decrement() {
    currentSeconds = secs % 60;
    
    if (currentSeconds <= Number(secs-1)) currentSeconds = "0" + currentSeconds;
    secs--;
   
     $("#bar").css("width", currentSeconds*secs+"%").text(currentSeconds);
    if (secs !== -1)
            timerStart();
    else if (secs < 0) {
        if(niveau==2){
           secs = 8;
        }else if(niveau==3){
           secs = 6;
        }
        if (i < count) {
            currentSeconds = 0;
            loadImage(link[i].url, lastPoint = null);
        }else if (i == count) {

            timerStop();
            put(urlPut);  
            i=i+1;
            greyOut();     
        }

    }
}

//Faire apparaitre pointeur sur la carte leaflet 
function popUp(map, lat, lng) {
    L.marker([lat, lng]).addTo(map);
}

app.controller('mapCtrl', function($scope, $rootScope,$http, getInfoPart, getInfoPhotos) {
    $scope.mapShow = function() {
        $rootScope.map;
        //Recevoir les infor de la partie
        $scope.infoPart = getInfoPart.infos().then(
            function(partData) {

                $scope.infoPart = partData;
                // Display cart
                niveau=$scope.infoPart.resultat.part.niveau;
                lat = $scope.infoPart.resultat.ville.lat;
                lng = $scope.infoPart.resultat.ville.lng;
                $rootScope.map = L.map('map').setView([lat, lng], 13);
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: ''
                }).addTo($rootScope.map);
            },
            function(msg) {
                alert(msg);
            });
        //Get photos
        $rootScope.infoPhotos = getInfoPhotos.getPhotos().then(
            function(PhotoData) {
                $rootScope.infoPhotos = PhotoData;
                 updateSeconds();   
                count = $rootScope.infoPhotos.part.photos.length;
                link = $rootScope.infoPhotos.part.photos;
                if (i == 0) loadImage(link[i].url);
                $("#score").html(Number(score));
                $rootScope.map.on('click', function(e) {
                   if(i <= count){
                    popUp($rootScope.map, e.latlng.lat, e.latlng.lng);
                         var lastPoint = e.latlng;
                         calculScore(lastPoint,secs);
                    }
                    timerStop();
                    if(link[i]){
                    loadImage(link[i].url, lastPoint);}
                    else{
                            if(i==count){
                                i=i+1;
                            }   
                            greyOut();
                            angular.injector(["MonApp"]).invoke(function (game) {
                              $http.put(game.url+"&score="+score+"&state=termine")
                                .success(function(res){
                                }).error(function(msg){
                                    alert(msg);
                                });
                            });
                      
                        } 
                         
                    
                });
            },
            function(msg) {
                alert(msg);
            });
    


        angular.injector(["MonApp"]).invoke(function (game) {
          urlPut = game.url;
        });
        

    }

});

//modifie le temps selon le niveau choisi 
function updateSeconds(){
    secs=10;
    if(niveau==2){
        secs = 8;
    }else if(niveau==3){
        secs = 6;
    }
    $("#bar").css("width","100%").attr('aria-valuemax', secs).text(secs);
}
//charge l'image aprés chaque clique ou aprés que le timer soit égale à 0 seconde
function loadImage(src, lastPoint) {
    $('#monImage').attr('src', dirname(location.pathname)+"/"+src);
    
    i++;
    timerStart();

}
//calcule le score des joueurs
function calculScore(lastPoint,temps){
    if (lastPoint != null) {
        var d=800;
         if(niveau==1){
            secs = 10
        }
        else  if(niveau==2){
           secs = 8;
             d = 600;
        }else if(niveau==3){
           secs = 6;
            d = 500;
        }
        currentSeconds = 0;
        var latlng = L.latLng(lat, lng);
        var distance = latlng.distanceTo(lastPoint);
        if(temps > (secs-2)){
          if(distance < d){
            score = (score + 5)*4;
          }
        else if(distance < 2*d)
            score = (score + 3)*4;
        else if(distance < 3*d)
            score = (score + 1)*4;
        }
        else if(temps > (secs-4)){
          if(distance < d)
            score = (score + 1)*2;
        else if(distance < 2*d)
            score = (score + 1)*2;
        else if(distance < 3*d)
            score = (score + 1)*2;
        }

        $("#score").html(Number(score));
    }
}


//récupére l'url 
function dirname(path) {
    return path.replace(/\\/g, '/')
        .replace(/\/[^\/]*\/?$/, '');
}

//grise le body aprés que le jeu soit terminé
  function greyOut(){
  var base_url = dirname(location.pathname);
  $(".leaflet-control-zoom").css("visibility", "hidden");
   $(".leaflet-control-attribution").css("visibility", "hidden");
   $(".fa").css("visibility", "hidden");
    $('#screen').css({  "display": "block", opacity: 0.7, "width":$(document).width(),"height":$(document).height()});
    if (score==0){
        $('#grey').css({'display': 'block'}).html("Pas de chance! vous avez perdu :( </br></br> <a id='lien' href="+base_url+"><img src="+base_url+"/App/template/images/restart-32.png"+"></img> Recommencer le jeu</a>" ); 
    }
    else{
      $('#grey').css({'display': 'block'}).html("Votre score est : " + score +" pts  </br></br> <a id='lien' href="+base_url+"><img src="+base_url+"/App/template/images/restart-32.png"+"></img> Continuer le fun !</a>" );   
    }
    
 }
 

