<?php
use Illuminate\Database\Capsule\Manager as DB;

$caps = new DB;
$config = parse_ini_file('config.ini');
$caps->addConnection( array(
'driver' => 'mysql',
'host' => $config['hostname'],
'database' => $config['dbname'],
'username' => $config['user'],
'password' => $config['password'],
'charset' => 'utf8',
'collation' => 'utf8_unicode_ci',
'prefix' => '',
));

$caps->setAsGlobal();

$caps->bootEloquent();