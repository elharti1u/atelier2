<?php


class AdminC {

    public function index($app) {
        if (!isset($_SESSION['login']['id'])) {
            $v = new AdminV();
            $v->display();
        } elseif ($_SESSION['login']['id'] == '1') {
            $url = $app->urlFor("addPhotos") . "?session=" . $_SESSION['login']['key'];
            $app->response()->redirect($url);
        }
    }

    private function crypter($pass, $salt) {
        return crypt($pass, $salt);
    }

    public function login($app) {
        if (!isset($_SESSION['login']['id'])) {
            $email = $app->request()->post("email");
            $password = $app->request()->post("password");
            $user = User::whereRaw("email = ?", array($email))->first();
            $v = new AdminV();
            if ($email == "" || $password == "") {
                $message = "Veuillez remplir tous les champs";
                $v->addVar("message", $message);
                $v->display();
            } elseif (count($user) != 0) {
                $salt = $user->salt;
                $pasCrypt = $this->crypter($password, $salt);
                if ($user->password == $pasCrypt) {
                    $random = rand(1000, 1000000);
                    $r = crypt($random);
                    $_SESSION['login'] = array("id" => "1","pseudo"=>$user->pseudo,"userId"=>$user->id, "key" => $r);
                    $url = $app->urlFor("addPhotos") . "?session=" . $r;
                    $app->response()->redirect($url);
                } else {
                    $message = "vos coordonnées n'ont pas été reconnues. veuillez recommencer";
                    $v->addVar("message", $message);
                    $v->display();
                }
            } else {
                $message = "vos coordonnées n'ont pas été reconnues. veuillez recommencer";
                $v->addVar("message", $message);
                $v->display();
            }
        } else {
            $url = $app->urlFor("addPhotos") . "?session=" . $_SESSION['login']['key'];
            $app->response()->redirect($url);
        }
    }

    public function inscriptionView() {
        $v = new InscriptionV();
        $v->display();
    }

    public function inscription($app) {

        $email = $app->request()->post("email");
        $password = $app->request()->post("password");
        $pseudo = $app->request()->post("pseudo");
        $password2 = $app->request()->post("password2");
        $user = User::whereRaw("email = ?", array($email))->first();
        if ($email == "" || $pseudo == "" || $password == "" || $password2 == "") {
            $v = new InscriptionV();
            $message = "Veuillez remplir tous les champs";
            $v->addVar("message", $message);
            $v->addVar("alert", "alert-danger");
            $v->display();
        } elseif (count($user) != 0) {
            $v = new InscriptionV();
            $message = "cette adresse email existe déjà";
            $v->addVar("message", $message);
            $v->addVar("alert", "alert-danger");
            $v->display();
        } elseif ($password2 != $password) {
            $v = new InscriptionV();
            $message = "les mots de passe saisis ne sont pas identiques";
            $v->addVar("message", $message);
            $v->addVar("alert", "alert-danger");
            $v->display();
        } else {
            $v = new InscriptionV();
            $u = new User();
            $u->pseudo = $pseudo;
            $u->email = $email;
            $salt = rand(100000, 10000000);
            $u->salt = $salt;
            $u->password = $this->crypter($password, $salt);
            $u->save();
            $message = 'votre compte a été créé avec succès';
            $v->addVar("message", $message);
            $v->addVar("alert", "alert-success");
            $v->display();
        }
    }
public function logout($app){
    session_destroy();
    $url=$app->urlFor('home');
    $app->response()->redirect($url);
    
}
}
