<?php


 
class AddPhotoC {

    public function index() {
        if (!isset($_SESSION['login']['id'])) {
            $v = new AdminV();
            $v->display();
        } elseif ($_SESSION['login']['id'] == '1') {
            $p = Photo::whereRaw("idUser = ?", array($_SESSION['login']['userId']))->orderBy('dateA', 'DESC')->get();
            if (count($p) > 0) {
                $v = new AddphotoV();
                $v->addVar("photos", $p);
                $v->display();
            } else {
                $v = new AddphotoV();
                $v->display();
            }
        }
    }

   public function addPhoto($app) {
        $name = $app->request()->post("name");
        $nameVille = $app->request()->post("localite");
        $lat = $app->request()->post("lat");
        $lng = $app->request()->post("lng");
        $v = new AddphotoV();
        if ($name == "" || $nameVille == "") {
            $p = Photo::whereRaw("idUser = ?", array($_SESSION['login']['userId']))->orderBy('dateA', 'DESC')->get();
            if (count($p) > 0) {
                $v->addVar("photos", $p);
            }
            $message = "veuillez saisir tous les champs";
            $v->addVar('message', $message);
            $v->addVar('alert', "alert-danger");
            $v->display();
        } else {
              $p = Photo::whereRaw("idUser = ?", array($_SESSION['login']['userId']))->orderBy('dateA', 'DESC')->get();
            if (count($p) > 0) {
                $v->addVar("photos", $p);
            }
            $ville = City::whereRaw("name = ?", array($nameVille))->first();
            if (count($ville) == 0) {
            	$opts = array('http' => array('proxy'=> 'tcp://www-cache.iutnc.univ-lorraine.fr:3128', 'request_fulluri'=> true));
				$context = stream_context_create($opts); 
                $jsonString = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=" . $nameVille,false,$context);
                $obj = json_decode($jsonString);
                if ($obj->status !== "ZERO_RESULTS") {
                    $latville = $obj->results[0]->geometry->location->lat;
                    $lngville = $obj->results[0]->geometry->location->lng;
                    $newV = new City();
                    $newV->name = $nameVille;
                    $newV->lat = $latville;
                    $newV->lng = $lngville;
                    $newV->save();
                    $p = new Photo();
                    $p->name = $nameVille;
                    $p->idCity = $newV->id;
                    $p->lat = $lat;
                    $p->lng = $lng;
                    $p->DateA = date('Y-m-d');
                    $p->idUser = $_SESSION['login']['userId'];
                    $gencode = rand(100000, 10000000000);
                    $urlimage = "App/template/images/" . $gencode . ".jpeg";
                    $img = \Gregwar\Image\Image::open($_FILES["image"]["tmp_name"][0]);
                    $img->forceResize(400, 400);
                    $img->jpeg();
                    $img->save($urlimage);
                    $p->url = $urlimage;
                    $p->save();
                    $message = "votre image a ete ajouté avec succès";
                    $v->addVar('message', $message);
                    $v->addVar('alert', "alert-success");
                    $app->response()->redirect($app->urlFor("addPhotos"));
                } else {
                    $message = "impossible d'ajouter la ville";
                    $v->addVar('message', $message);
                    $v->addVar('alert', "alert-success");
                    $v->display();
                }
            } else {
                $p = new Photo();
                $p->name = $nameVille;
                $p->idCity = $ville->id;
                $p->lat = $lat;
                $p->lng = $lng;
                $p->idUser = $_SESSION['login']['userId'];
                $p->DateA = date('Y-m-d');
                $gencode = rand(100000, 10000000000);
                $urlimage = "App/template/images/" . $gencode . ".jpeg";
                $img = \Gregwar\Image\Image::open($_FILES["image"]["tmp_name"][0]);
                $img->forceResize(400, 400);
                $img->jpeg();
                $img->save($urlimage);
                $p->url = $urlimage;
                $p->save();
                $message = "votre image a ete ajouté avec succès";
                $v->addVar('message', $message);
                $v->addVar('alert', "alert-success");
                $app->response()->redirect($app->urlFor("addPhotos"));
            }
        }
    }
	

}
