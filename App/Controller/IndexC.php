<?php


class IndexC {

    public function index() {
        $city = City::all();
        $v = new IndexV();
        if (count($city) > 0) {
            $v->addVar("villes", $city);
        }
        $v->display();
    }

    public function newPart($app) {
        $niveau1 = $app->request()->post('niveau');
        $pseudo = $app->request()->post('pseudo');
        $villeId = $app->request()->post('ville');
        if ($niveau1 == "" || $pseudo == "") {
            $city = City::all();
            $v = new IndexV();
            if (count($city) > 0) {
                $v->addVar("villes", $city);
            }
            $message = "Veuillez choisir un pseudo et un niveau !";
            $v->addVar("message", $message);
            $v->display();
        } else {
            $part = new Part();
            $part->pseudo_user = $pseudo;
            $part->idCity = $villeId;
            $random = rand(1000, 1000000);
            $r = crypt($random);
            $part->token = $r;
            $part->niveau=$niveau1;
            $part->dateC =date('y-m-d'); 
            $part->save();
            $id = $part->id;
            $_SESSION['token'] = array("id" => $id, "key" => $r);
            $url = $app->urlFor("getPartInfo", array("id" => $id)) . "?token=" . $r;
            $urlPhotos = $app->urlFor("getPartphotos", array("id" => $id)) . "?token=" . $r;
            $v = new GameV();
            $v->addVar('url', $url);
            $v->addVar('urlPhotos', $urlPhotos);
            $v->display();
        }
    }

}
