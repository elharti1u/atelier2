<?php

class GameC {

    public function index() {
        $v = new GameV();
        $v->display();
    }

     public function getPartInfo($app, $id) {
        $urlToken = $app->request()->get('token');
        if (isset($_SESSION['token']["key"]) && isset($_SESSION['token']["id"]) && !is_null($urlToken)) {
            $idSession = $_SESSION['token']["id"];
            $token = $_SESSION['token']["key"];
            if ($urlToken == $token && $idSession == $id) {
                $part = Part::find($id);
                $pa = array("part_id" => $part->id, "pseudo" => $part->pseudo_user,"niveau"=>$part->niveau);
                $ville = array("name" => $part->city->name, "lat" => $part->city->lat, "lng" => $part->city->lng);
                $app->response->headers->set('Content-type', 'application/json');
                $app->response()->setStatus(200);
                $resultat = array("resultat" => array("part" => $pa, "ville" => $ville, "status" => "ok"));
                echo json_encode($resultat);
            } else {
                $rep = array("status" => "ZERO_RESULTS");
                echo json_encode($rep);
            }
        } else {
            $rep = array("status" => "ZERO_RESULTS");
            echo json_encode($rep);
        }
    }

    public function getPartphotos($app, $id) {
        $urlToken = $app->request()->get('token');
        if (isset($_SESSION['token']["key"]) && isset($_SESSION['token']["id"]) && !is_null($urlToken)) {
            $idSession = $_SESSION['token']["id"];
            $token = $_SESSION['token']["key"];
            if ($urlToken == $token && $idSession == $id) {
                $part = Part::find($id);
                $photos = Photo::where("idCity", "=", $part->idCity)->take(10)->orderByRaw("RAND()")->get();
                foreach ($photos as $ph) {
                    $pho[] = array("id" => $ph->id, "url" => $ph->url, "name" => $ph->name, "lat" => $ph->lat, "lng" => $ph->lng);
                }
                $app->response->headers->set('Content-type', 'application/json');
                $app->response()->setStatus(200);
                $resultat = array("part" => array("photos" => $pho), "status" => "ok");
                echo json_encode($resultat);
            } else {
                $rep = array("status" => "ZERO_RESULTS");
                echo json_encode($rep);
            }
        } else {
            $rep = array("status" => "ZERO_RESULTS");
            echo json_encode($rep);
        }
    }


    public function updatePartInfo($app, $id) {
        $urlToken = $app->request()->params('token');
        if (isset($_SESSION['token']["key"]) && isset($_SESSION['token']["id"]) && !is_null($urlToken)) {
            $idSession = $_SESSION['token']["id"];
            $token = $_SESSION['token']["key"];
            if ($urlToken == $token && $idSession == $id) {
            
                $part = Part::find($id);
                if($part->state!='termine'){
                $part->score = $app->request()->params("score");
                $part->state = $app->request()->params("state");
                
                if($part->save()){
            
                $resultat = array("status" => "ok");
                echo json_encode($resultat);
            
             }else {
                $rep = array("status" => "ZERO_RESULTS");
                echo json_encode($rep);
            }
                }
               
        }
    }
             
    }

}
