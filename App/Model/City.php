<?php


class City extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'city';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function photos(){
        return $this->hasMany('Photo','idCity');
    }

}
