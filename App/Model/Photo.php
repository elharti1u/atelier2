<?php


class Photo extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function city(){
        return $this->belongsTo('City','idCity');
    }

    public static function getAll()
	{
		return photo::all()->toArray();

	}

}

