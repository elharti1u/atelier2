<?php

class Part extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'part';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function city() {
        return $this->belongsTo('City', 'idCity');
    }

    

}
