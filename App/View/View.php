<?php

abstract class View {

    protected $layout = null;
    protected $arrayVar;

    public function __construct() {
        
    }

    public function addVar($var, $val) {
        $this->arrayVar[$var] = $val;
    }

    public function render() {
        $app = Slim\Slim::getInstance();
        $urlBase = $app->request->getRootUri();
        $this->arrayVar['urlBase'] = $urlBase;
        $this->arrayVar['pageLoginForm'] = $app->urlFor("pageLoginForm");
        $this->arrayVar['home'] = $app->urlFor("home");
        $this->arrayVar['addPhotos'] = $app->urlFor("addPhotos");
         $this->arrayVar['UrlScore'] = $app->urlFor("UrlScore");
        $this->arrayVar['logout'] = $app->urlFor("logoutUrl");
        if (isset($_SESSION['login']['id'])) {
            $this->arrayVar['session'] = $_SESSION['login']['id'];
        } else {
            $this->arrayVar['session'] = '0';
        }
        $loader = new Twig_Loader_Filesystem('App/template/twig');
        $twig = new Twig_Environment($loader);
        $tmpl = $twig->loadTemplate($this->layout);
        return $tmpl->render($this->arrayVar);
    }

    public function display() {
        echo $this->render();
    }

}
