<?php

class AdminV extends View {
   
    public function __construct() {
        parent::__construct();
        $app = Slim\Slim::getInstance();
        $this->layout = 'identification.twig';
        $this->addVar('ConnexionUrlForm', $app->urlFor("ConnexionUrlForm"));
        $this->addVar("UrlInscription", $app->urlFor("inscriptionView"));
    }
    
}
