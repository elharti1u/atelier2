<?php

class AddphotoV  extends View{
      public function __construct() {
        parent::__construct();
        $app=  \Slim\Slim::getInstance();
        $this->addVar("viewAddPhotos", "1");
        $this->addVar("UrlAddPhoto",$app->urlFor("UrlAddPhoto"));
        $this->layout = 'addphoto.twig';
        
    }
}
