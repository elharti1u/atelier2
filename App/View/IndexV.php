<?php

class IndexV extends View {

    public function __construct() {
        parent::__construct();
        $app = Slim\Slim::getInstance();
        $this->layout = 'index.twig';
        if (isset($_SESSION['login']['pseudo'])) {
            $this->addVar('pseudo', $_SESSION['login']['pseudo']);
        }
        $this->addVar('actionUrl', $app->urlFor("newPart"));
    }

}
